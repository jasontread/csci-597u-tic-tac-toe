import { Table } from 'aws-cdk-lib/aws-dynamodb'

import { Stack, type StackProps } from 'aws-cdk-lib'

import type { Construct } from 'constructs'
import { PhpFunction, packagePhpCode } from '@bref.sh/constructs';
import path = require('path')
import { HttpMethod } from 'aws-cdk-lib/aws-events'
import { HttpLambdaIntegration } from 'aws-cdk-lib/aws-apigatewayv2-integrations'
import { HttpApi } from 'aws-cdk-lib/aws-apigatewayv2'
import * as iam from 'aws-cdk-lib/aws-iam'

/**
 * AppStack properties
 */
export interface AppStackProps extends StackProps {
  // DynamoDB game table
  readonly gameTable: Table
}

/**
 * The AppStack contains the S3 bucket for hosting of static web files, Lambda
 * function for game logic, and other supporting infrastructure components
 */
export class AppStack extends Stack {
  // NOTE: this is an ugly hack - would use SSM parameter in real world
  readonly LOGIN_URL = 'https://csci591-tic-tac-toe.auth.us-east-1.amazoncognito.com/login?client_id=4j1ua7u9hbe2sthg35ek3uj9j1&response_type=token&redirect_uri=https://nf79q8ypji.execute-api.us-east-1.amazonaws.com'

  // S3 bucket for this environment
  readonly api: HttpApi

  /**
   * Instantiates a new AppStack object
   * @param scope the stack scope
   * @param id the logical ID/prefix for this stack and its resources
   * @param props the stack properties
   */
  constructor (scope: Construct, id: string, props: AppStackProps) {
    super(scope, id, props)

    // Use the Bref CDK constructs to deploy a PHP Lambda function
    const lambda = new PhpFunction(this, `${id}Lambda`, {
      code: packagePhpCode(
        path.resolve(__dirname, '..', '..', '..', 'src', 'lambda')
      ),
      handler: 'index.php',
      memorySize: 128,
      functionName: `${id}Lambda`,
      environment: {
        GAME_TABLE: props.gameTable.tableName,
        LOGIN_URL: this.LOGIN_URL,
        REGION: 'us-east-1',
        SENDER_EMAIL: 'jason@read.is'
      }
    })

    // Create API Gateway for the Lambda function
    this.api = new HttpApi(this, `${id}HttpApi`, {
      apiName: 'TicTacToe API'
    })
    const lambdaIntegration = new HttpLambdaIntegration(
      `${id}HttpApiLambda`,
      lambda
    )
    this.api.addRoutes({
      path: '/',
      methods: [ HttpMethod.GET ],
      integration: lambdaIntegration,
    })
    this.api.addRoutes({
      path: '/auth',
      methods: [ HttpMethod.POST ],
      integration: lambdaIntegration,
    })
    this.api.addRoutes({
      path: '/game',
      methods: [ HttpMethod.POST ],
      integration: lambdaIntegration,
    })

    lambda.addEnvironment('API_ENDPOINT', this.api.apiEndpoint)

    // Grant RW access to the game table
    props.gameTable.grantReadWriteData(lambda)

    // Grant emailing permissions to the Lambda
    lambda.addToRolePolicy(
      new iam.PolicyStatement({
        effect: iam.Effect.ALLOW,
        actions: [
          'ses:SendEmail',
          'ses:SendRawEmail',
          'ses:SendTemplatedEmail',
        ],
        resources: [
          'arn:aws:ses:us-east-1:880121700735:identity/*',
          'arn:aws:ses:us-east-1:880121700735:configuration-set/my-first-configuration-set'
        ]
      }),
    )
  }
}
