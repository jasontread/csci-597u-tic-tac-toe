import * as cognito from 'aws-cdk-lib/aws-cognito'

import { CfnOutput, RemovalPolicy, Stack, type StackProps } from 'aws-cdk-lib'

import type { Construct } from 'constructs'

/**
 * CiamStackProps properties
 */
export interface CiamStackProps extends StackProps {
  // URL for the Tic Tac Toe API gateway
  readonly apiEndpoint: string
}

/**
 * The CiamStack stack contains Customer Identity and Access Management (CIAM)
 * related resources and settings
 */
export class CiamStack extends Stack {
  /**
   * Instantiates a new CiamStack object
   * @param scope the stack scope
   * @param id the logical ID/prefix for this stack and its resources
   * @param props the stack properties
   */
  constructor (scope: Construct, id: string, props: CiamStackProps) {
    super(scope, id, props)

    // Initialize Cognito UserPool
    const userPool = new cognito.UserPool(this, `${id}UserPool`, {
      accountRecovery: cognito.AccountRecovery.EMAIL_ONLY,
      autoVerify: { email: true },
      enableSmsRole: false,
      selfSignUpEnabled: true,
      signInAliases: { username: true, email: true },
      standardAttributes: {
        // Email address is a required standard attribute
        email: {
          required: true
        },
        // First and last name are required
        familyName: {
          required: true
        },
        givenName: {
          required: true
        }
      },
      removalPolicy: RemovalPolicy.DESTROY,
      userPoolName: `${id}UserPool`
    })

    // Initialize website client
    const client = userPool.addClient(`${id}UserPoolClient`, {
      userPoolClientName: `${id}UserPoolClient`,
      authFlows: {
        userPassword: true,
      },
      oAuth: {
        flows: {
          implicitCodeGrant: true,
        },
        callbackUrls: [ props.apiEndpoint ]
      },
    })

    // Initialize hosted UI using Cognito domain
    const domain = userPool.addDomain(`${id}UserPoolDomain`, {
      cognitoDomain: {
        domainPrefix: 'csci591-tic-tac-toe'
      }
    })
    const signInUrl = domain.signInUrl(client, {
      redirectUri: props.apiEndpoint
    })

    // Show sign-in URL
    new CfnOutput(this, `${id}HttpApiEndpoint`, { //NOSONAR
      description: 'Sign-in/Sign-up URL:',
      value: signInUrl
    })
  }
}
