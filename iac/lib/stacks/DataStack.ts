import { AttributeType, Table } from 'aws-cdk-lib/aws-dynamodb'

import { RemovalPolicy, Stack, type StackProps } from 'aws-cdk-lib'

import type { Construct } from 'constructs'

/**
 * The DataStack stack contains the data/persistence tier of the application
 */
export class DataStack extends Stack {
  // DynamoDB sessions table for this environment (if ec2_count > 1)
  readonly gameTable: Table

  /**
   * Instantiates a new DataStack object
   * @param scope the stack scope
   * @param id the logical ID/prefix for this stack and its resources
   * @param props the stack properties
   */
  constructor (scope: Construct, id: string, props: StackProps) {
    super(scope, id, props)

    this.gameTable = new Table(this, `${id}GameTable`, {
      partitionKey: {
        name: 'gameID',
        type: AttributeType.STRING
      },
      removalPolicy: RemovalPolicy.DESTROY,
      tableName: `${id}GameTable`
    })
  }
}
