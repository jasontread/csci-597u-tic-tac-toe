#!/usr/bin/env node
import 'source-map-support/register';
import * as cdk from 'aws-cdk-lib';
import { CiamStack } from '../lib/stacks/CiamStack';
import { DataStack } from '../lib/stacks/DataStack';
import { AppStack } from '../lib/stacks/AppStack';

const NAME = 'TicTacToe'
const REGION = 'us-east-1'

const app = new cdk.App()

const dataStack = new DataStack(app, `${NAME}Data`, {
  env: {
    region: REGION
  }
})

const appStack = new AppStack(app, `${NAME}App`, {
  env: {
    region: REGION
  },
  gameTable: dataStack.gameTable
})

const ciamStack = new CiamStack(app, `${NAME}Ciam`, {
  env: {
    region: REGION
  },
  apiEndpoint: appStack.api.apiEndpoint
})

appStack.addDependency(dataStack, 'AppStack needs the DDB game table')
ciamStack.addDependency(appStack, 'CiamStack needs the API Gateway URL')
