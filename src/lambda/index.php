<?php

declare(strict_types=1);

namespace tictactoe;

require_once(__DIR__ . '/vendor/autoload.php'); //NOSONAR

return new TicTacToeController();
