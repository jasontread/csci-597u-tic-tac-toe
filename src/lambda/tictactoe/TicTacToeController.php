<?php

declare(strict_types=1);

namespace tictactoe;

use Aws\DynamoDb\DynamoDbClient;
use Aws\Ses\SesClient;
use Bref\Context\Context;
use Bref\Event\Handler;

/**
 * Game controller class for Tic Tac Toe
 */
class TicTacToeController implements Handler {
  /**
   * DynamoDB client
   */
  private readonly DynamoDbClient $ddb;

  /**
   * DynamoDB table name
   *
   * @var string
   */
  private readonly string $table;

  /**
   * TicTacToeController constructor - established a connection for the DDB
   * table
   */
  public function __construct() {
    $this->table = getenv('GAME_TABLE');
  }

  /**
   * Lambda handler function
   *
   * @param mixed $event
   * @param Context $context
   * @return array
   */
  public function handle(mixed $event, Context $context): array {
    // GET request serves content from the ../web folder (404 if not found)
    if ($event['requestContext']['http']['method'] == 'GET') {
      return $this->serve($event);
    // POST is reserved for API methods
    } else {
      if (isset($event['body'])) {
        $raw = $event['isBase64Encoded']
          ? base64_decode($event['body'])
          : $event['body'];
        // JSON body
        if (preg_match('/{/', $raw)) {
          $data = json_decode($raw, true);
        // Standard HTTP form post body
        } else {
          parse_str($raw, $data);
        }
      } else {
        $data = [];
      }
      $method = substr($event['requestContext']['http']['path'], 1);
      return method_exists($this, $method) ? $this->$method($data) : null;
    }
  }

  /**
   * Authenticates a user
   *
   * @param array $data The POST body
   * @return array
   */
  private function auth(array $data): array { //NOSONAR
    // Poor mans authentication
    $valid = isset($data['token']) && preg_match('/token=/', $data['token']);
    return [
      'statusCode' => $valid ? 200 : 400,
      'body' => $valid ? rand(10000, 99999) : ''
    ];
  }

  /**
   * Creates a new game and storage in DDB
   *
   * @param string $gameID The ID of the game to create
   * @return array
   */
  private function createGame(string $gameID): array {
    $game = [0, 0, 0, 0, 0, 0, 0, 0, 0];
    $this->initDDB();
    $this->ddb->putItem([
      'Item' => [
        'gameID' => [ 'S' => $gameID ],
        'game' => [ 'S' => implode(',', $game) ]
      ],
      'TableName' => $this->table
    ]);
    return $game;
  }

  /**
   * Initialized DDB connection
   *
   * @return void
   */
  private function initDDB(): void {
    if (!isset($this->ddb)) {
      $this->ddb = new DynamoDbClient([ 'region' => getenv('REGION') ]);
    }
  }

  /**
   * Game handler function
   *
   * @param array $data The form post data
   * @return array
   */
  private function game(array $data): array { //NOSONAR
    $gameID = $data['gameID'] ?? null; //NOSONAR
    $game = isset($data['invite']) ? $this->createGame($gameID) : $this->loadGame($gameID); //NOSONAR
    if (isset($data['invite']) && preg_match('/@/', $data['invite'])) {
      $this->sendInvite($data['invite'], $gameID);
    }
    $playerID = $data['playerID'] ?? null; //NOSONAR
    // Update game
    if (isset($data['updateGame']) && $data['updateGame'] == 1) {
      $game = [];
      foreach (range(0, 8) as $i) {
        $game[] = $data['cell_' . $i] ?? 0;
      }
      $this->updateGame($gameID, $game);
    }
    $offTurn = (array_sum($game) > 0 && $playerID == $gameID) || //NOSONAR
      (array_sum($game) <= 0 && $playerID != $gameID);
    $thisPlayer = sprintf('Player %s', $playerID == $gameID ? 'A' : 'B'); //NOSONAR
    $otherPlayer = sprintf('Player %s', $playerID == $gameID ? 'B' : 'A'); //NOSONAR
    // Check if game over
    $check = [];
    $check[] = $game[0] + $game[1] + $game[2];
    $check[] = $game[3] + $game[4] + $game[5];
    $check[] = $game[6] + $game[7] + $game[8];
    $check[] = $game[0] + $game[3] + $game[6];
    $check[] = $game[1] + $game[4] + $game[7];
    $check[] = $game[2] + $game[5] + $game[8];
    $check[] = $game[0] + $game[4] + $game[8];
    $check[] = $game[2] + $game[4] + $game[6];
    // Player A wins
    if (in_array(3, $check)) {
      $winner = $playerID == $gameID; //NOSONAR
    } elseif (in_array(-3, $check)) {
      $winner = $playerID != $gameID;
    } else {
      $winner = null;
    }
    $gameOver = in_array(3, $check) || in_array(-3, $check); //NOSONAR
    $stalemate = array_sum(array_map('abs', $game)) == 9; //NOSONAR
    ob_start();
    include(sprintf('%s/web/game.php', dirname(__DIR__))); //NOSONAR
    $game = ob_get_contents();
    ob_end_clean();
    return [
      'statusCode' => 200,
      'headers' => ['Content-Type' => 'text/html'],
      'body' => $game
    ];
  }

  /**
   * Loads an existing game from DDB
   *
   * @param string $gameID The ID of the game to create
   * @return array
   */
  private function loadGame(string $gameID): array {
    $this->initDDB();
    $record = $this->ddb->getItem([
      'Key' => [ 'gameID' => [ 'S' => $gameID ] ],
      'ConsistentRead' => true,
      'TableName' => $this->table,
      'ProjectionExpression' => 'game'
    ]);
    return explode(',', $record['Item']['game']['S']);
  }

  /**
   * This method attempts to send a game invitation to the $email specified
   *
   * @param string $email The email to send to
   * @param string $gameID The game ID
   * @return void
   */
  private function sendInvite(string $email, string $gameID): void {
    $ses = new SesClient([
      'version' => '2010-12-01',
      'region' => getenv('REGION')
    ]);
    $ses->sendEmail([
      'Destination' => [
        'ToAddresses' => [$email]
      ],
      'ReplyToAddresses' => [getenv('SENDER_EMAIL')],
      'Source' => getenv('SENDER_EMAIL'),
      'Message' => [
      'Body' => [
          'Text' => [
              'Charset' => 'UTF-8',
              'Data' => sprintf(
                "Please join me for a fun game of Tic-Tac-Toe.\n\nTo get " .
                "go to %s and enter the Game ID %s.",
                getenv('API_ENDPOINT'),
                $gameID
              ),
          ],
        ],
        'Subject' => [
            'Charset' => 'UTF-8',
            'Data' => "Let's play Tic-Tac-Toe",
        ],
      ],
    ]);
  }

  /**
   * Used to serve static content from the ../web folder
   *
   * @param mixed $event The lambda event
   * @return array
   */
  private function serve(mixed $event): array {
    $file = sprintf(
      '%s/web%s',
      dirname(__DIR__),
      $event['requestContext']['http']['path'] == '/'
        ? '/index.html'
        : $event['requestContext']['http']['path']
    );
    return [
      'statusCode' => 200,
      'headers' => ['Content-Type' => 'text/html'],
      'body' => str_replace(
        'LOGIN_URL',
        getenv('LOGIN_URL'),
        file_get_contents($file)
      )
    ];
  }

  /**
   * Updates the game in DDB
   *
   * @param string $gameID The ID of the game to create
   * @param array $game The new game
   * @return array
   */
  private function updateGame(string $gameID, array $game): void {
    $this->initDDB();
    $this->ddb->putItem([
      'Item' => [
        'gameID' => [ 'S' => $gameID ],
        'game' => [ 'S' => implode(',', $game) ]
      ],
      'TableName' => $this->table
    ]);
  }
}
