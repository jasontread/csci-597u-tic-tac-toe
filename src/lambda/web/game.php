<?php

declare(strict_types=1);

?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title>Tic-Tac-Toe</title>
  <style>
  input[type=checkbox]
  {
    /* Double-sized Checkboxes */
    -ms-transform: scale(4); /* IE */
    -moz-transform: scale(4); /* FF */
    -webkit-transform: scale(4); /* Safari and Chrome */
    -o-transform: scale(4); /* Opera */
    transform: scale(4);
    padding: 40px;
  }
  </style>
  <?php if ($offTurn) : ?>
  <script>
  window.onload = function() {
    setTimeout(function() {
      document.getElementById('game').submit();
    }, 3000);
  };
  </script>
  <?php endif; ?>
</head>

<body>
  <h1>Welcome to Tic-Tac-Toe Game <?=$gameID?></h1>
  <?php if ($gameOver || $stalemate) : ?>
  <h2>Game Over!</h2>
    <?php if ($gameOver) : ?>
      <?=$winner ? 'You are the winner - congratulations!' : 'You lost - better luck next time!'?>
    <?php else : ?>
    <p>It was a stalemate!</p>
    <?php endif; ?>
  <hr>
  <a href="/">Play Again</a>
  <?php else : ?>
  <h2><?=$offTurn ? sprintf('Waiting on %s', $otherPlayer) : sprintf("It's your turn %s", $thisPlayer)?></h2>
  <form id="game" style="margin-left: 30px" method="post" action="/game">
    <input name="gameID" type="hidden" value="<?=$gameID?>" />
    <input id="playerID" name="playerID" type="hidden" value="<?=$playerID?>" />
    <input name="updateGame" type="hidden" value="<?=$offTurn ? 0 : 1?>" />
    <table style="width:300px;height:300px">
      <tr>
      <?php foreach ($game as $i => $cell) : ?>
        <?=$i == 3 || $i == 6 ? '</tr><tr>' : ''?>
        <td style="font-size:32px;font-weight:bold">
          <input
            name="cell_<?=$i?>"
            type="<?=$cell == 0 ? 'checkbox' : 'hidden'?>"
            value="<?=$cell == 0 ? ($gameID == $playerID ? 1 : -1) : $cell  //NOSONAR?>"
            <?=$offTurn
               ? 'disabled'
               : 'onclick="document.getElementById(\'game\').submit()"'?>
          >
          <?=$cell == 0 ? '' : ($cell == 1 ? 'X' : 'O') //NOSONAR?>
        </td>
      <?php endforeach; ?>
      </tr>
    </table>
  </form>
  <?php endif; ?>
</body>

</html>
