# Tic-Tac-Toe
This repo contains the source code for 
CSCI 597U [Assignment 3](https://wwu.instructure.com/courses/1681352/assignments/8790139)
implemented by Jason Read. Infrastructure components are deployed using the CDK
stacks defined in [iac/](iac/). Game source code is located in [src/](src/).

## Group Members
- Jason Read
- Xavier Cano (withdrew for quarter)

## Video Demo
The video demo and description of the AWS resources is available here:

[https://www.dropbox.com/scl/fi/2cg83qvwj97l2s6psw6fm/video-demo.mov?rlkey=qhq2ezpd3ysx7jtgmnkwwf8eh&dl=0](https://www.dropbox.com/scl/fi/2cg83qvwj97l2s6psw6fm/video-demo.mov?rlkey=qhq2ezpd3ysx7jtgmnkwwf8eh&dl=0)

## API Gateway + UI Link
Both the API and web UI are hosted by API Gateway + Lambda at this URL:

[https://nf79q8ypji.execute-api.us-east-1.amazonaws.com](https://nf79q8ypji.execute-api.us-east-1.amazonaws.com)

## Challenges
The most challenging part of the assignment was setting up the Lambda.

## Release Notes
The game does not end until every cell has been filled, even if the game is at
a stalemate.
